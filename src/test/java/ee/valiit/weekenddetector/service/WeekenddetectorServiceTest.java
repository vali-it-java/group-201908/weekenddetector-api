package ee.valiit.weekenddetector.service;

import org.junit.Test;
import org.springframework.util.Assert;

public class WeekenddetectorServiceTest {

    @Test
    public void testIsItWeekend() {
        WeekenddetectorService weekenddetectorService = new WeekenddetectorService();

        Assert.isTrue(weekenddetectorService.isItWeekend("2019-09-08 19:00"), "Incorrect weekend detection");
        Assert.isTrue(weekenddetectorService.isItWeekend("2019-09-09 05:59"), "Incorrect weekend detection");
        Assert.isTrue(!weekenddetectorService.isItWeekend("2019-09-09 06:01"), "Incorrect weekend detection");
        Assert.isTrue(!weekenddetectorService.isItWeekend("2019-09-09 09:35"), "Incorrect weekend detection");
        Assert.isTrue(weekenddetectorService.isItWeekend("2019-09-13 16:00"),"Incorrect weekend detection");
    }

//    @Test
//    public void testClickCounter() {
//        WeekenddetectorService weekenddetectorService = new WeekenddetectorService();
//
//        Assert.isTrue(weekenddetectorService.getVisitCount() == 1, "Incorrect visit count");
//        Assert.isTrue(weekenddetectorService.getVisitCount() == 2, "Incorrect visit count");
//        Assert.isTrue(weekenddetectorService.getVisitCount() == 3, "Incorrect visit count");
//        Assert.isTrue(weekenddetectorService.getVisitCount() == 4, "Incorrect visit count");
//        Assert.isTrue(weekenddetectorService.getVisitCount() == 5, "Incorrect visit count");
//        Assert.isTrue(weekenddetectorService.getVisitCount() == 6, "Incorrect visit count");
//    }
}
