package ee.valiit.weekenddetector.controller;

import ee.valiit.weekenddetector.model.WeekendTestResponse;
import ee.valiit.weekenddetector.service.WeekenddetectorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
public class WeekenddetectorController {

    @Autowired
    private WeekenddetectorService weekenddetectorService;

    @GetMapping("/isItWeekend")
    public WeekendTestResponse isItWeekend(@RequestParam("datetime") String dateTimeText) {
        int visitCount = weekenddetectorService.getVisitCount();
        boolean isWeekend = weekenddetectorService.isItWeekend(dateTimeText);
        if (isWeekend) {
            return new WeekendTestResponse("true", "No, you must not work right now!", visitCount);
        } else {
            return new WeekendTestResponse("false", "You are allowed to work right now!", visitCount);
        }
    }
}
