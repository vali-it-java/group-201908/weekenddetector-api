package ee.valiit.weekenddetector.model;

public class WeekendTestResponse {

    private String status;
    private String message;
    private int visitCount;

    public WeekendTestResponse() {
    }

    public WeekendTestResponse(String status, String message, int visitCount) {
        this.status = status;
        this.message = message;
        this.visitCount = visitCount;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public int getVisitCount() {
        return visitCount;
    }
}
