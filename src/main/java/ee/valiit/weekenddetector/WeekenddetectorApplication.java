package ee.valiit.weekenddetector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeekenddetectorApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeekenddetectorApplication.class, args);
	}

}
