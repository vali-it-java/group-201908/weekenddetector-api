package ee.valiit.weekenddetector.service;

import ee.valiit.weekenddetector.repository.WeekenddetectorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class WeekenddetectorService {

    @Autowired
    private WeekenddetectorRepository weekenddetectorRepository;

    public boolean isItWeekend(String dateTimeText) {
        LocalDateTime dateTime =
                LocalDateTime.parse(dateTimeText, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));

        if(dateTime.getDayOfWeek().getValue() == 6 ||dateTime.getDayOfWeek().getValue() == 7) {
            return true;
        } else if (dateTime.getDayOfWeek().getValue() == 1 && dateTime.getHour() < 6) {
            return true;
        } else if (dateTime.getDayOfWeek().getValue() == 5 && dateTime.getHour() >= 16) {
            return true;
        }
        return false;
    }

    public int getVisitCount() {
        weekenddetectorRepository.addVisit();
        return weekenddetectorRepository.getVisitCount();
    }
}
