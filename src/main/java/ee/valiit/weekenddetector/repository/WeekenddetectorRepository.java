package ee.valiit.weekenddetector.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class WeekenddetectorRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void addVisit() {
        jdbcTemplate.update("INSERT INTO visit () VALUES ();");
    }

    public int getVisitCount() {
        return jdbcTemplate.queryForObject("SELECT count(id) FROM visit;", Integer.class);
    }
}
